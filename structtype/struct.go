// You can also use dots with struct pointers - the pointers are automatically dereferenced.

package main

import "fmt"

type person struct {
	name string
	age  int
}

func main() {
	fmt.Println(person{"Bob", 20})
	fmt.Println(&person{"Ann", 40})

	s := person{"Sean", 50}
	sp := &s

	sp.age = 41
	fmt.Println(*sp)
}
