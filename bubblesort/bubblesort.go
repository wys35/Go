package main

import (
	"fmt"
	"math/rand"
)

var numbers [10]int

func initf () {
	for i:=0; i<10; i++ {
		numbers[i] = rand.Intn(100)
	}
}

func sort () {
	swapped := true
	j := 0
	var tmp int

	for swapped {
		swapped = false
		j++
		for i:=0; i<10-j; i++ {
			if numbers[i] > numbers[i+1] {
				tmp = numbers[i]
				numbers[i] = numbers[i+1]
				numbers[i+1] = tmp
				swapped = true
			}
		}
	}
}

func main () {
	initf()
	fmt.Println("before sorting")
	for i:=0; i<10; i++ {
		fmt.Println(numbers[i])
	}
	fmt.Println("sorting")
	sort()
	for i:=0; i<10; i++ {
		fmt.Println(numbers[i])
	}

}