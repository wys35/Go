// https://blog.wuyuansheng.com/2018/06/28/throttling-go-routines/

package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

var w sync.WaitGroup

func main() {
	ch := make(chan int, 25)

	for i := 0; i < 5; i++ {
		w.Add(1)
		go func(p int, c chan int) {
			for j := range c {
				time.Sleep(time.Millisecond)
				fmt.Println("process " + strconv.Itoa(p) + " reading " + strconv.Itoa(j))
			}
			w.Done()
		}(i, ch)
	}

	for z := 0; z < 25; z++ {
		ch <- z
	}
	close(ch)
	w.Wait()
}
